<!DOCTYPE html>
<html lang="es">

<!--  - - - - - - - - - - - - - - - - - - - - -CABECERA- - - - - - - - - - - - - - - - - - - - - - - - -->
<head>
  <meta charset="utf-8" />

  <title>Administración ITCH</title>

    <?php if (isset($output)):
      foreach($css_files as $file): ?>
        <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>
    <?php endif; ?>

    <?php if (isset($output)):
      foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>
    <?php endif; ?>


    <link rel="stylesheet" href="<?=base_url()?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>css/icomoon/style.css" />
    <link rel="stylesheet" href="<?=base_url()?>css/prettify.css" />
    <link rel="stylesheet" href="<?=base_url()?>css/jquery.pageslide.css" />      
    <link rel="stylesheet" href="<?=base_url()?>css/theme/style.css" />
    <link rel="stylesheet" href="<?=base_url()?>css/jquery.tablesorter.pager.css" />
    <link rel="stylesheet" href="<?=base_url()?>css/style/style.css" />

  <script src="<?=base_url()?>js/jquery.min.js"></script> 
  <script src="<?=base_url()?>js/bootstrap.min.js"></script>
  <script src="<?=base_url()?>js/madmin.js"></script>
  <script src="<?=base_url()?>js/application.js"></script>
  <script src="<?=base_url()?>js/excanvas.min.js"></script>
  <script src="<?=base_url()?>js/jquery.flot.js"></script>
  <script src="<?=base_url()?>js/jquery.flot.resize.min.js"></script>
  <script src="<?=base_url()?>js/jquery.flot.pie.min.js"></script>
  <script src="<?=base_url()?>js/prettify.js"></script>
  <script src="<?=base_url()?>js/jquery.tablesorter.min.js"></script>
  <script src="<?=base_url()?>js/jquery.pageslide.min.js"></script>
  <script src="<?=base_url()?>js/demo-area-chart.js"></script>
  <script src="<?=base_url()?>js/demo-dynamic-chart.js"></script>
  <script src="<?=base_url()?>js/demo-pie-chart.js"></script>
    
</head>
<!-- - - - - - - - - - - - - - - - - - - - - - FIN CABECERA - - - - - - - - - - - - - - - - - - - - - - -->

<body class="sidebar-max">


<ul id="topbar" class="on-click">
  <li class="pull-left">
    <h1 id="topbar-title">
      <i class="icon-cogs"></i>
      <a href="#">ADMIN Control Panel</a>
    </h1>
  </li>
  <li class="parent pull-right">
    <a href="#" data-toggle="dropdown">
      <i class="icon-user"></i>
      <span><?=$this->session->userdata('username')?></span>
    </a>
    <ul class="dropdown-menu">
      <li><a href="#">Profile</a></li>
      <li><a href="#">Finances</a></li>
      <li class="divider"></li>
      <li><a href="<?=base_url().'login/logout_ci'?>">Logout</a></li>
    </ul>
  </li>
  <li class="parent pull-right">
    <a href="#" data-toggle="dropdown">
      <i class="icon-cog"></i>
      <span>Settings</span>
    </a>
    <ul class="dropdown-menu">
      <li><a href="#">Backend Settings</a></li>
      <li><a href="#">Frontend Settings</a></li>
    </ul>
  </li>
</ul>


<div class="container-fluid" id="container">

    <div id="sidebar">

      <div class="search-mini-wrapper">
        <form action="#" class="search-mini">
          <input name="search" type="text" placeholder="  Search..." />
          <button type="submit">
            <i class="icon-search"></i>
          </button>
        </form>
      </div>      

      <ul class="sidebar-menu on-click" id="main-menu">
        <li class="active">
          <div class="sidebar-menu-item-wrapper">
            <a href="<?=base_url()?>admin">
              <i class="icon-home"></i>
              <span>Dashboard</span>
            </a>
          </div>
        </li>
        <li class="inactive">
          <div class="sidebar-menu-item-wrapper">
            <a href="#">
              <i class="icon-book"></i>
              <span>Stats &amp; Reports</span>
            </a>
          </div>
        </li>
        <li class="parent inactive">
          <div class="sidebar-menu-item-wrapper">
            <a href="#">
              <i class="icon-database"></i>
              <span>Database</span>
            </a>
            <ul>
              <li><a href="#"><i class="icon-cog"></i> <span>Settings</span></a></li>
              <?php foreach ($this->db->list_tables() as $table): ?>
                  <li><a href="<?= base_url()?>admin/<?= $table ?>"><i class="icon-database"></i><span><?= $table ?></span></a></li>
              <?php endforeach; ?>
            </ul>
          </div>
        </li>
        <li class="inactive">
          <div class="sidebar-menu-item-wrapper">
            <a href="#">
              <i class="icon-dashboard"></i>
              <span>Virtual Machines</span>
            </a>
          </div>
        </li>
        <li class="parent inactive">
          <div class="sidebar-menu-item-wrapper">
            <a href="#">
              <i class="icon-users"></i>
              <span>Users</span>
            </a>
            <ul>
              <li><a href="#">User Accounts</a></li>
              <li><a href="#">User settings</a></li>
              <li><a href="#">Special user settings</a></li>
            </ul>
          </div>
        </li>
      </ul>


      <a href="#" id="sidebar-resizer">
        <i class="min icon-expand"></i>
        <i class="max icon-maximize"></i>
      </a>

    </div>

    <div id="content">

      <div class="page" id="page-dashboard">

        <div class="panel" id="panel-101">
          <header>
            <i class="icon-asterisk"></i>
            <span>Shortcuts</span>
          </header>
          <div class="content tiles-container">
            
            <a href="#" class="tile-btn"><i class="icon-stats"></i><span>Stats</span></a>
            <a href="<?= base_url() ?>admin/database" class="tile-btn"><i class="icon-database"></i><span>Database</span></a>
            <a href="#" class="tile-btn"><i class="icon-dashboard"></i><span>VMs</span></a>
            <a href="<?=base_url()?>admin/users" class="tile-btn"><i class="icon-users"></i><span>Users</span></a>

            <a href="#" class="tile-btn"><i class="icon-screen2"></i><span>Monitoring</span></a>
            <a href="#" class="tile-btn"><i class="icon-flag"></i><span>Notices</span></a>
            <a href="#" class="tile-btn"><i class="icon-film"></i><span>Media</span></a>

          </div>
        </div>

        <div class="panel" id="panel-101">
          <?php if (isset($output)) {
            echo "<div>";
            echo $output;
            echo "</div>";
          }
          ?>

          <?php if (isset($tables)): ?>
            <?php foreach ($tables as $tbl): ?>
              <a class="btn btn-default" href="<?= base_url() ?>admin/<?= $tbl ?>" role="button"><?= $tbl ?></a>
            <?php endforeach; ?>
          <?php endif; ?>
        </div>

      
      </div>

    </div>
    <!-- /#content: The Main Content Section -->

</div><!-- /.container-fluid#container -->

</body>
</html>