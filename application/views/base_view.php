<!doctype html>
<html lang="es">
<head> 
	<meta charset="utf-8">
	<title>Instituto Tecnológico de Chetumal</title>

	<link rel="stylesheet" type="text/css" media="all" href="<?=base_url()?>css/metro.css" />
	<link rel="styleshhet" type="text/css" href="<'=base_url()css/icomoon/style.css"/>

	<script src="<?=base_url()?>js/jquery.min.js"></script>
	<script src="<?=base_url()?>js/jquery.plugins.min.js"></script>
	<script src="<?=base_url()?>js/metro.js"></script>


</head> 
<body>
	<div class="metro-layout horizontal">
		<div class="header">
			<h1>Instituto Tecnológico de Chetumal</h1>
		</div>
		<div class="content clearfix">
			<div class="items">
				<a class="box" href="<?=base_url().'login/logout_ci'?>">
					<span>LogOut</span>
					<img class="icon" src="" alt="" />
				</a>
				<a class="box" href="#" style="background: #6b6b6b;">
					<span>Settings</span>
					<img class="icon" src="<?=base_url()?>img/settings.png" alt="" />
				</a>
				<a class="box width2 height2" href="#">
					<span></span>
					<img class="cover" src="" alt="" />
				</a>
				<a class="box height2" href="#" style="background: #d32c2c;">
					<span></span>
					<img class="icon big" src="" alt="" />
				</a>
			</div>
		</div>
	</div>
</body>
</html>
