<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class User_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}

	function nuevoUsuario($username, $password, $perfil) {
		$this ->db->insert('users', array('username'=>$username, 'password'=>$password, 'perfil'=>$perfil));
	}

	function eliminarUsuario($username){
		$this ->db->delete('users', array('username'=>$username));
	}

	function updateUsuario($username, $password){
		$this ->db->update('users', array('username' => $username, 'password' => $password));
	}

	function updateUsername($username, $newusername){
		$this->db->where('username', $username);
		$this->db->update('users', array('username'=>$newusername));
	}

	function updatePassword($password, $newpassword){
		$this->db->where('password', $password);
		$this->db->update('users', array('password'=>$newpassword));
	}

	function getUser($username){
		$this->db->where('username',$username);
		$query = $this->db->get('users');
		if($query->num_rows() > 0) return $query;
		else return false;
	}
	function getUsers(){
		$query = $this->db->get('users');
		if($query->num_rows() > 0) return $query;
		else return false;
	}
}