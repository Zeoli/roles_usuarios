<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Admin extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('Grocery_CRUD');
		$this->load->database();
	}
	
	public function index()
	{
		if($this->session->userdata('perfil') == FALSE || $this->session->userdata('perfil') != 'administrador')
		{
			redirect(base_url().'login');
		}
		$this->load->view('win_view');
	}

	public function database()
	{
		$data = array(
			'tables' => $this->db->list_tables()
			);

		$this->load->view('win_view', $data);
	}

	function _users_output($output = null)
	{
		$crud = $this->load->view('win_view', $output);
	}

	public function users()
	{
		$crud = new Grocery_CRUD();

		$crud->set_table('users');
		$crud->columns('username', 'perfil', 'nombre', 'apellido');
		$crud->fields('username', 'perfil', 'password', 'nombre', 'apellido');

		$crud->field_type('password', 'password');

		$crud->display_as('username', 'Nombre de Usuario')
			->display_as('nombre', 'Nombre')->display_As('apellido', 'Apellidos');

		$crud->set_relation('perfil', 'rol', 'rol');
		$crud->set_subject('Usuario');
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_read();

		$output = $crud->render();

		$this->_users_output($output);
	}

	public function consulta()
	{
		$crud = new Grocery_CRUD();

		$crud->set_table('consulta');
		$crud->columns('Clv Presupuesto', 'Clv Cuentas Contables', 'Clv Movimientos', 'Clv Gastos', 'Descripcion');

		$crud->display_as('Clv presupuesto', 'Presupuesto')->display_as('Clv Cuentas contables','Cuentas Contables')
			->display_as('Clv Movimientos', 'Movimientos')->display_as('Clv Gastos', 'Gastos');

		$crud->set_subject('Consultas');

		$output = $crud->render();

		$this->_users_output($output);
	}

	public function costes_organizativos()
	{
		$crud = new Grocery_CRUD();

		$crud->set_table('costes_organizativos');
		$crud->columns('Direccion', 'Administracion', 'Gastos Generales', 'Total Mensual', 'Total Anual', 'Total');

		$crud->set_subject('Costes');

		$output = $crud->render();

		$this->_users_output($output);
	}

	public function costos_operaciones()
	{
		$crud = new Grocery_CRUD();

		$crud->set_table('costos_operaciones');
		$crud->columns('Actividad', 'Costes de Unidad', 'Cantidad', 'Coste Total Articulo', 'Coste Total por Actividad');

		$crud->set_subject('Costos');

		$output = $crud->render();

		$this->_users_output($output);
	}

	public function cuentas_contables()
	{
		$crud = new Grocery_CRUD();

		$crud->set_table('cuentas_contables');
		$crud->columns('Reales o de Valores', 'de valuacion', 'Transitorias', 'De orden', 'Nominales', 'Cuentas Reales');

		$crud->set_subject('Cuentas');

		$output = $crud->render();

		$this->_users_output($output);
	}

	public function gastos()
	{
		$crud = new Grocery_CRUD();

		$crud->set_table('gastos');
		$crud->columns('Gastos de Proyecto', 'Gastos Totales');

		$crud->set_subject('Gastos');

		$output = $crud->render();

		$this->_users_output($output);
	}

	public function movimientos()
	{
		$crud = new Grocery_CRUD();

		$crud->set_table('movimientos');
		$crud->columns('Concepto', 'Tipo', 'Clv Presupuesto', 'Fecha');

		$crud->display_as('Clv Presupuesto', 'Presupuesto');
		$crud->set_subject('Movimientos');

		$output = $crud->render();

		$this->_users_output($output);
	}

	public function presupuesto()
	{
		$crud = new Grocery_CRUD();

		$crud->set_table('presupuesto');
		$crud->columns('Descripcion', 'Fecha', 'Clv Costes de Operaciones', 'Clv Costes Organizativos', 'Total');

		$crud->display_as('Clv Costes de Operaciones', 'Costes de Operacion')
			->display_as('Clv Costes Organizativos', 'Costes Organizativos');
		$crud->set_subject('Presupuesto');

		$output = $crud->render();

		$this->_users_output($output);
	}

	public function registro_egresos()
	{
		$crud = new Grocery_CRUD();

		$crud->set_table('registro_egresos');
		$crud->columns('Poliza', 'Clv Presupuesto', 'monto', 'Fecha', 'Concepto', 'Presupuesto de Egresos por Ejercer',
			'Presupuesto de Egresos Autorizado', 'Presupuesto de Egresos Comprometido', 'Total de Egreso');

		$crud->display_as('Clv Presupuesto', 'Presupuesto');
		$crud->set_subject('Registro de Egreso');

		$output = $crud->render();

		$this->_users_output($output);
	}

	public function registro_ingreso()
	{
		$crud = new Grocery_CRUD();

		$crud->set_table('registro_ingreso');
		$crud->columns('Donaciones', 'Ingresos Generados', 'Inversiones', 'Presupuesto de Ingresos por Recibir',
			'Presupuesto de Ingresos Autorizado', 'Presupuesto de Ingresos Devengados',
			'Presupuesto de Ingresos Cobrados');

		$crud->set_subject('Registro de Ingreso');

		$output = $crud->render();

		$this->_users_output($output);
	}

	public function rol()
	{
		$crud = new Grocery_CRUD();

		$crud->set_table('rol');
		$crud->columns('rol');

		$crud->display_as('rol', 'Perfil');
		$crud->set_subject('Perfil');

		$output = $crud->render();

		$this->_users_output($output);
	}
}
