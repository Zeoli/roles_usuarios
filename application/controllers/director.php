<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Director extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->database('default');
	}
	
	public function index()
	{
		if($this->session->userdata('perfil') == FALSE || $this->session->userdata('perfil') == 'editor')
		{
			redirect(base_url().'login');
		}
		$data = array('block_botones' => $this->load->view('editor_view') );
		$this->parser->parse('base_view',$data);
	}
}